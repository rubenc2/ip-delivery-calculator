document.getElementById('downloadResults').addEventListener('click', async function() {
    const { jsPDF } = window.jspdf;
    const doc = new jsPDF();

    const projectName = document.getElementById('projectName').value.trim();
    const filename = projectName || 'default-analysis'; // Default name if none provided

    // Fetch and add logo
    const logo = await fetch('logo.png').then(res => res.blob()).then(blob => {
        return new Promise((resolve) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob);
        });
    });

    doc.addImage(logo, 'PNG', 5, 5, 50, 12, 'ANY_ALIAS', 'NONE'); //logo dimentions in the pdf document
    let yPosition = 40; // Start writing text below the image

    // Create an instance of Intl.NumberFormat for formatting
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'decimal',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });

    function formatValue(value) {
        const num = parseFloat(value.replace(/,/g, ''));
        return isNaN(num) ? '0.00' : formatter.format(num);
    }

    // Collecting all data
    doc.setFontSize(12);
    const satelliteUse = document.getElementById('satelliteUse').value;
    const ipDelivery = document.getElementById('ipDeliveryModel').value;
    let ipDeliveryMode = ""; // Ensure variable is accessible
    if (ipDelivery === "numStreams") {
        ipDeliveryMode = "Number of Streams";
    } else if (ipDelivery === "gbIngest") {
        ipDeliveryMode = "Ingress Traffic (Channel)";
    } else if (ipDelivery === "gbEgress") {
        ipDeliveryMode = "Egress Traffic";
    }
    doc.setFontSize(14);
    doc.text(`Project Name: ${filename}`, 10, yPosition);
    yPosition +=16
    doc.setFontSize(12);
    doc.text(`IP Delivery Option: ${ipDeliveryMode}`, 10, yPosition);
    yPosition += 10;
    doc.text(`Setup Cost per location: ${formatValue(document.getElementById('ipSupCost').value)}`, 10, yPosition);
    yPosition += 10;
    doc.text(`Redundancy: ${document.getElementById('redundancy').checked ? 'Yes' : 'No'}`, 10, yPosition);
    yPosition += 10;
    doc.text(`Stream Bitrate (Mbps): ${formatValue(document.getElementById('streamBitrate').value)}`, 10, yPosition);
    yPosition += 10;
    doc.text(`Cost of GB: ${formatValue(document.getElementById('ipCost').value)}`, 10, yPosition);
    yPosition += 10;
    doc.text(`Cost per stream/year: ${formatValue(document.getElementById('costPerStream').value)}`, 10, yPosition);
    yPosition += 16;
    doc.text(`Satellite Use: ${satelliteUse}`, 10, yPosition);
    yPosition += 10;

    if (satelliteUse !== "onlyIP") {
        doc.text(`Transponder Cost per year: ${formatValue(document.getElementById('satCost').value)}`, 10, yPosition);
        yPosition += 10;
        doc.text(`Uplink Cost per year: ${formatValue(document.getElementById('upCost').value)}`, 10, yPosition);
        yPosition += 10;
        doc.text(`Infrastructure recurrent cost (year): ${formatValue(document.getElementById('inCostYear').value)}`, 10, yPosition);
        yPosition += 10;
        doc.text(`Infrastructure one time cost: ${formatValue(document.getElementById('inCostOne').value)}`, 10, yPosition);
        yPosition += 10;
        doc.text(`Downlink cost per location: ${formatValue(document.getElementById('dCost').value)}`, 10, yPosition);
        yPosition += 10;
    }
    
    yPosition += 6;
    doc.text(`Number of Locations: ${formatValue(document.getElementById('numLocations').value)}`, 10, yPosition);
    yPosition += 16;

    // Results
    doc.setFontSize(14);
    doc.text("RESULTS:", 10, yPosition);
    yPosition += 10;
    doc.setFontSize(14);
    doc.text(`IP Capex: ${formatValue(document.getElementById('ipCapex').textContent)}`, 10, yPosition);
    yPosition += 10;
    doc.text(`IP Opex MRC: ${formatValue(document.getElementById('ipOpex').textContent)}`, 10, yPosition);
    if (satelliteUse !== "onlyIP") {
        yPosition += 10;
        doc.text(`Satellite Capex: ${formatValue(document.getElementById('satCapex').textContent)}`, 10, yPosition);
        yPosition += 10;
        doc.text(`Satellite Opex MRC: ${formatValue(document.getElementById('satOpex').textContent)}`, 10, yPosition);
    }

    // Save the PDF
    doc.save(`${filename}-results.pdf`);
});

