#!/bin/bash
#docker installer
#===========================================================
# Docker-CE & docker-compose - Installer v1.1
# Ruben Calzadilla
#===========================================================
#
#Option
opt=$1
#
# Set environment
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Clear the screen
clear

echo "-----------------------------------"
echo " ___       _       _           _"
echo "|_ _|_ __ | |_ ___| |___  __ _| |_"
echo " | || '_ \| __/ _ \ / __|/ _\` | __|"
echo " | || | | | ||  __/ \__ \ (_| | |_"
echo "|___|_| |_|\__\___|_|___/\__,_|\__|"
echo " "
echo "     Welcome to the docker-ce &    "
echo "      docker-compose installer     "
echo "-----------------------------------"
echo " "


if [ -z $opt ]; then
	docker ps -a > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo " "
		echo "docker-ce is already installed"
		echo " "
		exit 1
	else
		#Installation logs
		lpath=`pwd`
		LOG=$lpath/docker_installer.log

		# Do we have Internet access
		timeout 3 ping -c1 8.8.8.8 > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo "No Internet access is detected, Please check!"
			echo "We need Internet access to install dependencies"
			echo "and to download and install the docker-ce engine application"
			echo " "
			exit 1
		fi

		### install Dependencies here
		echo "Installing Dependencies, this will take few minutes"
		echo " "
		#
		echo "Identifying OS"
		#
		# RHEL / CentOS / etc
		if [ -n "$(command -v yum)" ]; then
		    echo "Installing for RHEL/CentOS"
		    sudo yum -y install yum-utils >> $LOG 2>&1
		    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo >> $LOG 2>&1
		    sudo yum -y install docker-ce docker-ce-cli containerd.io docker-compose >> $LOG 2>&1
		    sudo systemctl start docker >> $LOG 2>&1
		fi
		#
		# Debian / Ubuntu
		if [ -n "$(command -v apt-get)" ]; then
		    echo "Installing for Debian/Ubuntu"
		    sudo apt-get update -y >> $LOG 2>&1
		    sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release >> $LOG 2>&1
		    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg >> $LOG 2>&1
		    sudo echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null >> $LOG 2>&1
		    sudo apt-get update -y >> $LOG 2>&1
		    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose >> $LOG 2>&1
		fi

		sudo groupadd docker >> $LOG 2>&1
		sudo usermod -aG docker $USER >> $LOG 2>&1
		echo " "
		echo "The Installation is completed"
		echo "The user [ $USER ] has been added to the -docker- group"
		echo "If you want to add another user, please issue the command [sudo usermod -aG docker <USER>]"
		echo "Now you need to log out and log back in for the changes to take place"
		echo "When back, run the script with the -t flag for a quick test"
		echo " "
		opt="-q"
	fi
fi

if [ $opt = "-t" ]; then
    sudo docker run hello-world
elif [ $opt = "-q" ]; then
	exit 0
else
	echo " "
	echo "You need to add a -t to run the test"
	echo "example: $./install_docker.sh -t"
	echo " "
fi
exit 0

