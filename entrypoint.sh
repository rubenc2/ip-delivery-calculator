#!/bin/sh

# Directory for SSL certificate and key
SSL_DIR="/etc/ssl/private"
SSL_CERT="/etc/ssl/certs/nginx-selfsigned.crt"
SSL_KEY="$SSL_DIR/nginx-selfsigned.key"

# Ensure the SSL directory exists
mkdir -p "$SSL_DIR"

# Generate a new self-signed SSL certificate
echo "Generating a new self-signed SSL certificate..."
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout "$SSL_KEY" \
    -out "$SSL_CERT" \
    -subj "/C=US/ST=State/L=City/O=Organization/CN=*"

# Start Nginx in the foreground
exec nginx -g "daemon off;"

