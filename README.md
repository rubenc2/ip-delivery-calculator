# IP-Delivery-Calculator


## Description
This is an IP-Delivery Calculator, the calculator will allow for the user to enter the IP delivery charging model with the option to compare with an existing satellite model. The calculator will then provide the Cost Analisys. The calculator is mainly written in JavaScript.<br>
When the docker container is started, it creates a 365 days self-signed certificate, the only port open is 443. There is a warning about un-trusted certificate when opening the page.<br>
The main scope of the calculator is to offer a generic ip-delivery cost estimate for the sales engineer or sales director to clearly show the best possible option to potential customers.

## Getting started
You can modify the existing files before running the docker build command to make any branding adjustments. You can also have the option to pull the image from the dockerhub registry, run the container, make any adjustments and comit the changes to your own image/registry.

Build your own:

First you need to install docker by executing the install_docker.sh shell script depending on your OS. This install id for any Linux distribution<br>
`chmod +x install_docker.sh`<br>
`./install_docker.sh`

Run this command from same directory where the files are.<br>
`docker build -t [imagename] .`

You can run the container.<br>
`docker run -d --name [appname] --restart unless-stopped -p443:443 [imagename]`

If you prefer to run an existing image, then:<br>
`docker run -d --name acalc --restart unless-stopped -p443:443 rubenc2/ipsatcalc:latest`

## Working Demo
[This is a working demo app](https://52.91.66.214/index.html)<br>
The username is: IntelsatMedia<br>
The password is: IntelsatMediaRocks!
<br>
## Authors and acknowledgment
Ruben Calzadilla<br>

## License
MIT License apply.

## Project status
The project is live and looking to improve it by adding more features or fixing issues
