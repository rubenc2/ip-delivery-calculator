document.addEventListener('DOMContentLoaded', function() {
    const ipDeliveryModelSelect = document.getElementById('ipDeliveryModel');
    const satelliteUseSelect = document.getElementById('satelliteUse');
    const numLocationsSlider = document.getElementById('numLocations');
    const numLocationsInput = document.getElementById('numLocationsInput');
    const redundancyCheckbox = document.getElementById('redundancy');
    const dCostInput = document.getElementById('dCost');
    const satelliteInputs = document.getElementById('satelliteInputs');
    const commonInputs = document.getElementById('commonInputs');
    const gbInputs = document.getElementById('gbInputs');
    const streamInputs = document.getElementById('streamInputs');

    function updateVisibilityBasedOnSelection() {
        // Initially hide all sections
        commonInputs.style.display = 'none';
        gbInputs.style.display = 'none';
        streamInputs.style.display = 'none';
        satelliteInputs.style.display = 'none';

        // Update based on current selections
        handleDeliveryModelChange();
        handleSatelliteUseChange();
    }

    function handleDeliveryModelChange() {
        commonInputs.style.display = ['gbIngest', 'gbEgress', 'numStreams'].includes(ipDeliveryModelSelect.value) ? 'block' : 'none';
        gbInputs.style.display = ['gbIngest', 'gbEgress'].includes(ipDeliveryModelSelect.value) ? 'block' : 'none';
        streamInputs.style.display = ipDeliveryModelSelect.value === 'numStreams' ? 'block' : 'none';
    }

    function handleSatelliteUseChange() {
        const selectedValue = satelliteUseSelect.value;
        satelliteInputs.style.display = (selectedValue === 'yes' || selectedValue === 'no') ? 'block' : 'none';
        dCostInput.disabled = selectedValue !== 'no';
    }

    function performCalculations() {
        const N = parseInt(numLocationsInput.value, 10) || 0; // Number of locations
        let ipSupCost = parseFloat(document.getElementById('ipSupCost').value) || 0; // Setup cost
        let redundancyFactor = document.getElementById('redundancy').checked ? 2.5 : 1; // Redundancy
        ipSupCost *= redundancyFactor; // Adjust setup cost based on redundancy

        // Satellite-specific inputs
        const SatCost = parseFloat(document.getElementById('satCost').value) || 0; // Satellite cost
        const UpCost = parseFloat(document.getElementById('upCost').value) || 0; // Uplink cost
        const InCostYear = parseFloat(document.getElementById('inCostYear').value) || 0; // Yearly infrastructure cost
        const InCostOne = parseFloat(document.getElementById('inCostOne').value) || 0; // One-time infrastructure cost
        let DCost = satelliteUseSelect.value === 'yes' ? 0 : parseFloat(document.getElementById('dCost').value) || 0; // Downlink cost

        // IP delivery model-specific inputs and calculations
        let streamBitrate, ipCost, sBR, CoPS, gbPerYearCost;
        if (ipDeliveryModelSelect.value !== 'numStreams') {
            streamBitrate = parseFloat(document.getElementById('streamBitrate').value) || 0; // Mbps
            ipCost = parseFloat(document.getElementById('ipCost').value) || 0; // Price per GB
            sBR = streamBitrate * 3942; // Convert Mbps to GB/year using corrected conversion
            gbPerYearCost = (ipDeliveryModelSelect.value === 'gbIngest' ? sBR : sBR * N) * ipCost / 12; // Monthly cost
        } else {
            CoPS = parseFloat(document.getElementById('costPerStream').value) || 0; // Cost per stream
            gbPerYearCost = CoPS * N / 12; // Monthly cost when based on number of streams
        }

        // Calculating Capex and Opex for IP and Satellite
        let ipCapex = N * ipSupCost;
        let ipOpexMRC = gbPerYearCost;
        let satelliteCapex = InCostOne + (N * DCost); // Capex includes downlink cost only if not using satellite
        let satelliteOpexMRC = (SatCost + UpCost + InCostYear) / 12; // Satellite monthly cost, static regardless of N if satellite is used

        const formatter = new Intl.NumberFormat('en-US');

        // Displaying the results
        document.getElementById('satCapex').textContent = formatter.format(satelliteCapex.toFixed(2));
        document.getElementById('satOpex').textContent = formatter.format(satelliteOpexMRC.toFixed(2));
        document.getElementById('ipCapex').textContent = formatter.format(ipCapex.toFixed(2));
        document.getElementById('ipOpex').textContent = formatter.format(ipOpexMRC.toFixed(2));

        document.getElementById('satelliteResult').style.display = 'block';
        document.getElementById('ipResult').style.display = 'block';
    }

    function clearAllInputs() {
        document.querySelectorAll('select').forEach(select => select.selectedIndex = 0);
        document.querySelectorAll('input[type=number], input[type=checkbox], input[type=text]').forEach(input => {
            if (input.type === 'checkbox') {
                input.checked = false;
            } else {
                input.value = '';  // clear text inputs like the project name
            }
            input.disabled = false;
        });
        numLocationsSlider.value = 0;
        numLocationsInput.value = 0;

        updateVisibilityBasedOnSelection();
        performCalculations();
    }

    // Correct event listener assignments for slider and input number of locations
    numLocationsSlider.addEventListener('input', () => {
        numLocationsInput.value = numLocationsSlider.value;
        performCalculations();
    });

    numLocationsInput.addEventListener('input', () => {
        // Ensure slider matches input when within range
        if (parseInt(numLocationsInput.value, 10) <= parseInt(numLocationsSlider.max, 10)) {
            numLocationsSlider.value = numLocationsInput.value;
        }
        performCalculations();
    });

    ipDeliveryModelSelect.addEventListener('change', handleDeliveryModelChange);
    satelliteUseSelect.addEventListener('change', handleSatelliteUseChange);
    document.getElementById('clear').addEventListener('click', clearAllInputs);

    // Initialize form on page load
    clearAllInputs();
});