# Use Alpine Linux as the base image
FROM alpine:latest

# Install Nginx and OpenSSL
RUN apk add --update nginx openssl

# Create a directory for the application
RUN mkdir -p /var/www/app

# Set the working directory to Nginx
WORKDIR /etc/nginx

# Remove the default Nginx configuration (if it exists)
RUN if [ -f /etc/nginx/conf.d/default.conf ]; then rm /etc/nginx/conf.d/default.conf; fi

# Copy the Nginx configuration and web content
COPY nginx.conf /etc/nginx/nginx.conf
COPY favicon.ico favicon.png index.html styles.css script.js download.js logo.png /var/www/app/

# Copy the entrypoint script
COPY entrypoint.sh /entrypoint.sh

# Make sure your entrypoint script is executable
RUN chmod +x /entrypoint.sh

# Expose port 443 for HTTPS
EXPOSE 443

# Use the entrypoint script to initialize things
ENTRYPOINT ["/entrypoint.sh"]

# Command to start Nginx in the foreground
CMD ["nginx", "-g", "daemon off;"]

